{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
{-|
Module      : Elab
Description : Elabora un término fully named a uno locally closed.
Copyright   : (c) Mauro Jaskelioff, Guido Martínez, 2020.
License     : GPL-3
Maintainer  : mauro@fceia.unr.edu.ar
Stability   : experimental

Este módulo permite elaborar términos y declaraciones para convertirlas desde
fully named (@NTerm) a locally closed (@Term@) 
-}

module Elab ( elab, elab_decl, desugarT, desugarDecl, desugarTy ) where
import Common (Pos)
import Lang
import Subst
import MonadFD4
import Global(tyDcl)

-- Desugaring de declaraciones
desugarDecl ::  MonadFD4 m => Decl SDecl -> m (Term, Ty)
desugarDecl (Decl p _ (LetFun False bs ty t)) = do
    t' <- desugarT t
    bs' <- mapM (desugarBs p) bs
    let dst = dsLam p bs' t'
        tt = elab dst
    ty' <- desugarTy p (makeFun (map snd bs) ty)
    return (tt, ty')
desugarDecl (Decl p x (LetFun True  bs ty t)) =
  do mf1 <- desugarTy p (makeFun (map snd bs) ty)
     bs' <- mapM (desugarBs p) bs
     dt  <- desugarT t
     let dst = Fix p x mf1 (fst $ head bs) (snd $ head bs') (dsLam p (tail bs') dt)
         tt = elab dst
     return (tt, mf1)

-- Desugaring de tipos
desugarTy :: MonadFD4 m => Pos -> STy -> m Ty
desugarTy _ SNatTy = return NatTy
desugarTy p (SFunTy t1 t2) =  do dst1 <- desugarTy p t1
                                 dst2 <- desugarTy p t2
                                 return $ FunTy dst1 dst2
desugarTy p (SNamedTy n) = do t <- lookupTyDcl n
                              case t of
                                  Just t' -> return (NamedTy n t')
                                  Nothing -> failPosFD4 p $ "Error: El tipo con nombre " ++ n ++ " no está declarado."

-- Desugaring de tipos para bindings
desugarBs :: MonadFD4 m => Pos -> (Name, STy) -> m (Name,Ty)
desugarBs p (n,sty) = do
    dsty <- desugarTy p sty
    return (n,dsty)

-- Desugaring de terminos
desugarT :: MonadFD4 m => STerm -> m NTerm
desugarT (SV info var) = return (V info var)
desugarT (SConst p v) = return (Const p v)
desugarT (SApp p (SPrint str) t) = Print p str <$> desugarT t
desugarT (SApp p a b) = do
  t1 <- desugarT a
  t2 <- desugarT b
  return (App p t1 t2)
desugarT (SLam info bs t) = do
    bs' <- mapM (desugarBs info) bs
    case bs' of
      [] -> failPosFD4 info "Error: Let sin declaraciones."
      _ ->  dsLam info bs' <$> desugarT t
--desugarT (SPrint str) = \x -> Print str x
desugarT (SBinaryOp info op a b) = do
                                    a' <- desugarT a
                                    b' <- desugarT b
                                    return (BinaryOp info op a' b')
desugarT (SFix info n1 t1 n2 t2 t) = do
                                      (n1', t1') <- desugarBs info (n1, t1)
                                      (n2', t2') <- desugarBs info (n2, t2)
                                      t' <- desugarT t
                                      return (Fix info n1' t1' n2' t2' t')
desugarT (SIfZ info b t1 t2) = do
                                  b' <- desugarT b
                                  t1' <- desugarT t1
                                  t2' <- desugarT t2
                                  return (IfZ info b' t1' t2')
desugarT (SLetFun info False n [] tp t1 t2) = do
                                        ty' <- desugarTy info tp
                                        t1' <- desugarT t1
                                        t2' <- desugarT t2
                                        return (Let info n ty' t1' t2')
desugarT (SLetFun info False f bs fty t1 t2) = do
                                          let ts = makeFun (map snd bs) fty in
                                            desugarT $ SLetFun info False f (tail bs) ts (SLam info bs t1) t2
desugarT (SLetFun info True f [(v, ty)] fty t1 t2) = desugarT $ SLetFun info False f [] (SFunTy ty fty) (SFix info f (SFunTy ty fty) v ty t1) t2
desugarT (SLetFun info True f (b:bs) fty t1 t2) = do
                                              let ts  = makeFun (map snd bs) fty
                                              desugarT (SLetFun info True f [b] ts (SLam info bs t1) t2)

-- Funcion auxiliar para 
dsLam :: t -> [(Name, Ty)] -> Tm t var -> Tm t var
dsLam _ [] t = t
dsLam p [(v,ty)] t = Lam p v ty t
dsLam p ((v,ty):bs) t = Lam p v ty (dsLam p bs t)

-- Calculo el tipo de una funcion 
makeFun :: [STy] -> STy -> STy
makeFun [] ty = ty
makeFun [t] ty = SFunTy t ty
makeFun (t:ts) ty = SFunTy t (makeFun ts ty)

-- | 'elab' transforma variables ligadas en índices de de Bruijn
-- en un término dado. 
elab :: NTerm -> Term
elab  = elab' []

elab' :: [Name] -> NTerm -> Term
elab' env (V p v) =
  -- Tenemos que hver si la variable es Global o es un nombre local
  -- En env llevamos la lista de nombres locales.
  if v `elem` env
    then  V p (Free v)
    else V p (Global v)

elab' _ (Const p c) = Const p c
elab' env (Lam p v ty t) = Lam p v ty (close v (elab' (v:env) t))
elab' env (Fix p f fty x xty t) = Fix p f fty x xty (closeN [f, x] (elab' (x:f:env) t))
elab' env (IfZ p c t e)         = IfZ p (elab' env c) (elab' env t) (elab' env e)
-- Operador Print
elab' env (Print i str t) = Print i str (elab' env t)
-- Operadores binarios
elab' env (BinaryOp i o t u) = BinaryOp i o (elab' env t) (elab' env u)
-- Aplicaciones generales
elab' env (App p h a) = App p (elab' env h) (elab' env a)
elab' env (Let p v vty def body) = Let p v vty (elab' env def) (close v (elab' (v:env) body))

elab_decl :: Decl NTerm -> Decl Term
elab_decl = fmap elab
