{-|
Module      : Parse
Description : Define un parser de términos FD40 a términos fully named.
Copyright   : (c) Mauro Jaskelioff, Guido Martínez, 2020.
License     : GPL-3
Maintainer  : mauro@fceia.unr.edu.ar
Stability   : experimental

-}

module Parse (tm, Parse.parse, decl, runP, P, program, declOrTm) where

import Prelude hiding ( const )
import Lang
import Common
import Text.Parsec hiding (runP,parse)
import Data.Char ( isNumber, ord )
import qualified Text.Parsec.Token as Tok
import Text.ParserCombinators.Parsec.Language --( GenLanguageDef(..), emptyDef )
import qualified Text.Parsec.Expr as Ex
import Text.Parsec.Expr (Operator, Assoc)
import Control.Monad.Identity (Identity)
import Control.Monad.RWS.Strict (MonadState(put))

type P = Parsec String ()

-----------------------
-- Lexer
-----------------------
-- | Analizador de Tokens
lexer :: Tok.TokenParser u
lexer = Tok.makeTokenParser $
        emptyDef {
         commentLine    = "#",
         reservedNames = ["let", "rec", "fun", "fix", "then", "else","in",
                           "ifz", "print","Nat"],
         reservedOpNames = ["->",":","=","+","-"]
        }

whiteSpace :: P ()
whiteSpace = Tok.whiteSpace lexer

natural :: P Integer
natural = Tok.natural lexer

stringLiteral :: P String
stringLiteral = Tok.stringLiteral lexer

parens :: P a -> P a
parens = Tok.parens lexer

identifier :: P String
identifier = Tok.identifier lexer

reserved :: String -> P ()
reserved = Tok.reserved lexer

reservedOp :: String -> P ()
reservedOp = Tok.reservedOp lexer

-----------------------
-- Parsers
-----------------------

num :: P Int
num = fromInteger <$> natural

var :: P Name
var = identifier

getPos :: P Pos
getPos = do pos <- getPosition
            return $ Pos (sourceLine pos) (sourceColumn pos)

tyvar :: P Name
tyvar = Tok.lexeme lexer $ do
 c  <- upper
 cs <- option "" identifier
 return (c:cs)

tyatom :: P STy
tyatom = (reserved "Nat" >> return SNatTy)
         <|> parens typeP
         <|> (SNamedTy <$> tyvar)

typeP :: P STy
typeP = try (do
          x <- tyatom
          reservedOp "->"
          SFunTy x <$> typeP)
      <|> tyatom

typeFun :: P STy
typeFun = try (do
          x <- tyatom
          reservedOp ":"
          SFunTy x <$> tyatom)
      <|> tyatom

const :: P Const
const = CNat <$> num

printOp :: P STerm
printOp = do
  i <- getPos
  reserved "print"
  str <- option "" stringLiteral
  a <- atom
  return (SPrint str)

binary :: String -> BinaryOp -> Assoc -> Operator String () Identity STerm
binary s f = Ex.Infix (reservedOp s >> return (SBinaryOp NoPos f))

table :: [[Operator String () Identity STerm]]
table = [[binary "+" Add Ex.AssocLeft,
          binary "-" Sub Ex.AssocLeft]]

expr :: P STerm
expr = Ex.buildExpressionParser table tm

atom :: P STerm
atom =     flip SConst <$> const <*> getPos
       <|> flip SV <$> var <*> getPos
       <|> parens expr
       <|> printOp

-- parsea un par (variable : tipo)
binding :: P (Name, STy)
binding = do v <- var
             reservedOp ":"
             ty <- typeP
             return (v, ty)

-- lam :: P STerm
-- lam = do i <- getPos
--          reserved "fun"
--          (v,ty) <- parens binding
--          reservedOp "->"
--          t <- expr
--          return (Lam i v ty t)

binding':: [(Name, STy)] -> P [(Name, STy)]
binding' l = do (v,ty) <- parens binding
                let l' = l ++ [(v, ty)] in binding' l'
                <|> return l

slam :: P STerm
slam = do i <- getPos
          reserved "fun"
          l <- binding' []
          reservedOp "->"
          SLam i l <$> expr

bindingFun :: P [(Name, STy)]
bindingFun = do v <- var
                l' <- parens binding
                reservedOp ":"
                ty <- typeFun
                return ((v,ty) : [l'])

-- Nota el parser app también parsea un solo atom.
app :: P STerm
app = do i <- getPos
         f <- atom
         args <- many atom
         return (foldl (SApp i) f args)

ifz :: P STerm
ifz = do i <- getPos
         reserved "ifz"
         c <- expr
         reserved "then"
         t <- expr
         reserved "else"
         SIfZ i c t <$> expr

fix :: P STerm
fix = do i <- getPos
         reserved "fix"
         (f, fty) <- parens binding
         (x, xty) <- parens binding
         reservedOp "->"
         SFix i f fty x xty <$> expr

letFun :: P STerm
letFun = do
  pos <- getPos
  reserved "let"
  bool <- optionalS
  v <- var
  b <- many0Binders
  reservedOp ":"
  ty <- typeP
  reservedOp "="
  t <- expr
  try (reserved "in")
  SLetFun pos bool v b ty t <$> expr


--declType :: P (Decl SDecl)
--declType = do
--  i <- getPos
--  reserved "type"
--  v <- var
--  reservedOp "="
--  Decl i v . TypeDecl v <$> typeP

-- | Parser de términos
tm :: P STerm
tm = app <|> slam <|> ifz <|> printOp <|> fix <|> letFun

-- | Parser de declaraciones
decl :: P (Decl SDecl)
decl = try declType <|> declLet


declLet :: P (Decl SDecl)
declLet = do
     i <- getPos
     reserved "let"
     b <- optionalS
     v <- var
     bs <- many0Binders
     reservedOp ":"
     ty <- typeP
     reservedOp "="
     Decl i v . LetFun b bs ty <$> expr

declType :: P (Decl SDecl)
declType = do
  i <- getPos
  reserved "type"
  v <- var
  reservedOp "="
  Decl i v . TypeDecl v <$> typeP

optionalS :: P Bool
optionalS = try (do reserved "rec"
                    return True) 
                    <|> return False

-- | Parser de programas (listas de declaraciones) 
program :: P [Decl SDecl]
program = many decl -- Hay que cambiar esto para que la decl de typo no sea solo al principio del archivo.

-- | Parsea una declaración a un término
-- Útil para las sesiones interactivas
declOrTm :: P (Either (Decl SDecl) STerm)
declOrTm =  try (Right <$> expr) <|> Left <$> decl

-- Corre un parser, chequeando que se pueda consumir toda la entrada
runP :: P a -> String -> String -> Either ParseError a
runP p s filename = runParser (whiteSpace *> p <* eof) () filename s

--para debugging en uso interactivo (ghci)
parse :: String -> STerm
parse s = case runP expr s "" of
            Right t -> t
            Left e -> error ("no parse: " ++ show s)

-- Utilidades de Syntactic Sugar.
manyBinders :: P [(Name, STy)]
manyBinders = do
  bs <- many1 (parens binders)
  return (concat bs)

many0Binders :: P [(Name, STy)]
many0Binders = do
  bs <- many (parens binders)
  return (concat bs)

binders :: P [(Name, STy)]
binders = do v <- many1 var
             reservedOp ":"
             ty <- typeP
             return (map (\x -> (x,ty)) v)
